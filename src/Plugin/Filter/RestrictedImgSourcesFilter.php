<?php

namespace Drupal\restricted_img_sources_filter\Plugin\Filter;

use Drupal\filter\FilterProcessResult;
use Drupal\filter\Plugin\FilterBase;
use Drupal\Component\Utility\Html;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a filter to restriction of image sources.
 *
 * @Filter(
 *   id = "restricted_img_sources_filter",
 *   title = @Translation("Restrict &lt;img&gt; tag sources that are not hosted on this site"),
 *   description = @Translation("Allows restricted usage of &lt;img&gt; tag sources that are not hosted on this site (you need to create a whitelist of allowed hosts)."),
 *   type = Drupal\filter\Plugin\FilterInterface::TYPE_TRANSFORM_IRREVERSIBLE,
 *   weight = 9
 * )
 */
class RestrictedImgSourcesFilter extends FilterBase {

  /**
   * {@inheritdoc}
   */
  public function process($text, $langcode) {

    $allowed_hosts = preg_split('/\r\n|\r|\n/', $this->settings['allowed_hosts']);

    $site_host = \Drupal::request()->getHost();
    $html_dom = Html::load($text);
    $images = $html_dom->getElementsByTagName('img');

    foreach ($images as $image) {
      $src = $image->getAttribute('src');
      $image_host = parse_url($src, PHP_URL_HOST);
      // If image is hosted on the current site.
      if ($image_host == NULL || $image_host == $site_host) {
        continue;
      }
      elseif (in_array($image_host, $allowed_hosts)) {
        continue;
      }

      // Allow modules and themes to replace an invalid image with an error
      // indicator. See filter_filter_secure_image_alter().
      \Drupal::moduleHandler()->alter('filter_secure_image', $image);
    }

    $text = Html::serialize($html_dom);
    return new FilterProcessResult($text);
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {

    $form['allowed_hosts'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Hosts that can be used as image sources.'),
      '#description' => $this->t('Enter each host at new line without protocols, like this: <br> www.drupal.org <br> upload.wikimedia.org'),
      '#default_value' => $this->settings['allowed_hosts'] ?? '',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function tips($long = FALSE) {
    return $this->t('Only images hosted on allowed sites may be used in &lt;img&gt; tags (see the whitelist of allowed sites in the filter settings).');
  }

}
